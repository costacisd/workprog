import keyboard as kbd
import win32clipboard
import json
from tkinter import *
from tkinter.filedialog import askopenfilename
import threading
import time

global text1
global text2
global text3
global text4
global text5
global text6
global text7

text1 = ''
text2 = ''
text3 = ''
text4 = ''
text5 = ''
text6 = ''
text7 = ''

def clear_site(index):

    working_dict[sites[index]]['attributes'].update({
        "category": "",
        "currency": "",
        "isbn": "",
        "name": "",
        "pictures": "",
        "price": "",
        "vendor": ""})

    working_dict[sites[index]]['cart'].update({
        "multiplyItemsPrice": "",
        "urlTemplate": "",
        "titles": "",
        "quantities": "",
        "prices": "",
        "totalPrices": "",
        "currency": ""})

    working_dict[sites[index]]['checkout'].update({
        "urlTemplate": "",
        "placeOrderBtn": ""})

    working_dict[sites[index]]['urlTemplates'] = []
    working_dict[sites[index]]['productPageSelector'] = ''
    working_dict[sites[index]]['mainPageTemplates'] = []
    clear_comments(index)


def get_clipboard():
    win32clipboard.OpenClipboard()
    data = win32clipboard.GetClipboardData()
    win32clipboard.EmptyClipboard()
    win32clipboard.CloseClipboard()
    return data


def set_clipboard(text):
    win32clipboard.OpenClipboard()
    win32clipboard.EmptyClipboard()
    win32clipboard.SetClipboardText(text, win32clipboard.CF_TEXT)
    win32clipboard.CloseClipboard()


def empty_clipboard():
    win32clipboard.OpenClipboard()
    try:
        win32clipboard.EmptyClipboard()
    except:
        pass
    win32clipboard.CloseClipboard()


def get_dict(path):

    with open(path, 'r', encoding='utf-8') as opened_file:
        data_dict = json.load(opened_file)
    opened_file.close()
    return data_dict


def set_dict(path, data_dict):

    with open(path, 'w', encoding='utf-8') as opened_file:
        opened_file.write(json.dumps(data_dict, ensure_ascii=False))
    opened_file.close()
    return True


def press_key():

    while True:
        key = kbd.read_hotkey(False)
        if key == 'ctrl+1':
            kbd.press_and_release('ctrl+c')
            time.sleep(0.2)
            data = get_clipboard()
            return [1, data]
        elif key == 'ctrl+2':
            return [2]
        elif key == 'ctrl+3':
            return [3]
        elif key == 'ctrl+q':
            return [0]


def press_key2():

    while True:
        key = kbd.read_hotkey(False)
        if key == 'ctrl+1':
            return [1]
        elif key == 'ctrl+2':
            return [2]
        elif key == 'ctrl+3':
            return [3]


def write_comment(text, index):

    working_dict[sites[index]]['meta']['comments'].append(text)


def remove_comment(text, index):
    try:
        working_dict[sites[index]]['meta']['comments'].remove(text)
    except ValueError:
        pass


def clear_comments(index):

    working_dict[sites[index]]['meta']['comments'] = []


def delete_comment(com_type, index, jn, kn):
    if com_type == 'status':
        if jn == 1:
            remove_comment('не работает', index)
        else:
            remove_comment('нет товаров', index)

    elif com_type == 'noPage':
        remove_comment('нет страницы ' + category[jn], index)

    elif com_type == 'inCategory':
        remove_comment('нет ' + name[str(jn) + str(kn)] + ' в ' + category[jn], index)

    elif com_type == 'aboutCategory':
        remove_comment('нет ' + category[jn], index)

    elif com_type == 'wave':
        remove_comment(category[jn] + ' выплывающим окном', index)

    elif com_type == 'empty':
        pass

    elif com_type == 'urltmpl':
        remove_comment('url не меняется после оформления заказа', index)

def comment(com_type, index, jn, kn):

    if com_type == 'status':
        if jn == 1:
            write_comment('не работает', index)
        else:
            write_comment('нет товаров', index)

    elif com_type == 'noPage':
        write_comment('нет страницы ' + category[jn], index)

    elif com_type == 'inCategory':
        write_comment('нет ' + name[str(jn) + str(kn)] + ' в ' + category[jn], index)

    elif com_type == 'aboutCategory':
        write_comment('нет ' + category[jn], index)

    elif com_type == 'wave':
        write_comment(category[jn] + ' выплывающим окном', index)

    elif com_type == 'empty':
        pass

    elif com_type == 'urltmpl':
        write_comment('url не меняется после оформления заказа', index)


category = {
    0: 'meta',
    1: 'attributes',
    2: 'urlTemplates',
    3: 'productPageSelector',
    4: 'mainPageTemplates',
    5: 'cart',
    6: 'checkout'
}


name = {
    '00': 'status',
    '10': 'category',
    '11': 'name',
    '12': 'pictures',
    '13': 'price',
    '14': 'currency',
    '15': 'vendor',
    '16': 'isbn',
    '50': 'urlTemplate',
    '51': 'titles',
    '52': 'quantities',
    '53': 'prices',
    '54': 'totalPrices',
    '55': 'currency',
    '60': 'placeOrderBtn',
    '61': 'urlTemplate'
}

category_num = {
    0: 1,
    1: 7,
    2: 0,
    3: 0,
    4: 0,
    5: 6,
    6: 2
}


class MyPrinter:

    done = 0
    all_sites = 0

    def __init__(self, all2):
        self.all_sites = all2

    def set(self, txt2, txt3, txt4, txt5, txt6, txt7, done1=-1):
        global text1
        global text2
        global text3
        global text4
        global text5
        global text6
        global text7

        if done1 != -1:
            self.done = done1

        text1 = str(self.done+1) + ' из ' + str(self.all_sites)
        text2 = txt2
        text3 = txt3
        text4 = txt4
        text5 = txt5
        text6 = txt6
        text7 = txt7


class Logic(threading.Thread):
    def run(self):

        with open(count_path, 'a+', encoding='utf-8') as count_file:
            count_file.seek(0)
            count = count_file.read()
        count_file.close()
        if count == "":
            count = 0
        count = int(count)
        i = 0
        while i < len(sites):

            if working_dict[sites[i]][category[0]][name['00']] == 'failed':
                j = 0
                flag = True
                flag2 = False
                while j < 19:
                    if j == 0:
                        printer1.set('ctrl+v', sites[i], 'ok', count, 'back', working_dict[sites[i]][category[0]][name['00']], done1=i)
                        set_clipboard(sites[i])

                        answer_1 = press_key2()[0]

                        if answer_1 == 1:
                            empty_clipboard()
                            working_dict[sites[i]][category[0]][name['00']] = 'ok'
                        elif answer_1 == 2:
                            empty_clipboard()
                            working_dict[sites[i]][category[0]][name['00']] = 'unsupported'
                            working_dict[sites[i]].pop(category[2])
                            printer1.set('unsupported', 'choose comment', 'not work', 'no goods', 'back', ' ')
                            answer_2 = press_key2()[0]

                            if answer_2 == 1:
                                comment('status', i, 1, 0)
                                break

                            elif answer_2 == 2:
                                comment('status', i, 2, 0)
                                break

                            else:
                                j = 0
                                continue
                        else:
                            working_dict[sites[i - 1]][category[0]][name['00']] = 'failed'
                            i -= 1
                            flag = False
                            break

                    if 0 < j < 8:
                        printer1.set(category[1], name[str(9+j)], 'copy', 'comment', 'back', working_dict[sites[i]][category[1]][name[str(9+j)]])
                        if j < 5:
                            delete_comment('inCategory', i, 1, j-1)
                        answer_3 = press_key()

                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[1]][name[str(9+j)]] = answer_3[1]
                            count += 1
                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[1]][name[str(9+j)]] = ''
                            if j < 5:
                                comment('inCategory', i, 1, j-1)
                            else:
                                comment('empty', i, 1, j-1)

                        elif answer_3[0] == 0:
                            j += 1
                            continue
                        else:
                            j -= 1
                            continue

                    if j == 8:
                        printer1.set('', category[2], 'copy', 'comment', 'back', working_dict[sites[i]][category[2]])
                        delete_comment('aboutCategory', i, 2, 0)
                        #working_dict[sites[i]][category[2]] = []
                        answer_3 = press_key()

                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[2]].append(answer_3[1])
                            count += 1
                            flagpps = False
                            j = 11
                            continue

                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[2]] = []
                            comment('aboutCategory', i, 2, 0)
                            working_dict[sites[i]].pop(category[2])
                            flagpps = True

                        elif answer_3[0] == 0:
                            j += 1
                            flagpps = True
                            continue

                        else:
                            j -= 1
                            count -= 1
                            continue
                    if j == 9:
                        printer1.set('', category[3], 'copy', 'comment', 'back', working_dict[sites[i]][category[3]])
                        delete_comment('aboutCategory', i, 3, 0)
                        answer_3 = press_key()

                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[3]] = answer_3[1]
                            count += 1
                            j = 11
                            continue

                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[3]] = ''
                            comment('aboutCategory', i, 3, 0)
                            j = 11
                            continue
                        
                        elif answer_3[0] == 0:
                            j = 11

                        else:
                            j -= 1
                            count -= 1
                            continue
                    if j == 10:
                        printer1.set('', category[4], 'copy', 'comment', 'back', working_dict[sites[i]][category[4]][0])
                        working_dict[sites[i]][category[4]] = []
                        answer_3 = press_key()

                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[4]].append(answer_3[1])

                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[4]] = []
                            comment('empty', i, 4, 0)

                        else:
                            j -= 1
                            continue
                    if j == 11:
                        printer1.set(category[5], name['50'], 'copy', 'comment', 'back', working_dict[sites[i]][category[5]][name['50']])
                        delete_comment('noPage', i, 5, 0)
                        delete_comment('wave', i, 5, 0)
                        flag2 = False
                        answer_3 = press_key()
                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[5]][name['50']] = answer_3[1]
                            count += 1
                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[5]][name['50']] = ''
                            printer1.set(category[5], 'choose comment', 'no page', 'wave', 'tiu', working_dict[sites[i]][category[5]][name['50']])
                            flag2 = True
                            answer_4 = press_key2()

                            if answer_4[0] == 1:
                                comment('noPage', i, 5, 0)
                                j = 17
                                working_dict[sites[i]][category[5]][name['51']] = ''
                                working_dict[sites[i]][category[5]][name['52']] = ''
                                working_dict[sites[i]][category[5]][name['53']] = ''
                                working_dict[sites[i]][category[5]][name['54']] = ''
                                working_dict[sites[i]][category[5]][name['55']] = ''
                                working_dict[sites[i]][category[5]]['multiplyItemsPrice'] = ''
                                continue

                            elif answer_4[0] == 2:
                                comment('wave', i, 5, 0)
                                j = 17
                                working_dict[sites[i]][category[5]][name['51']] = ''
                                working_dict[sites[i]][category[5]][name['52']] = ''
                                working_dict[sites[i]][category[5]][name['53']] = ''
                                working_dict[sites[i]][category[5]][name['54']] = ''
                                working_dict[sites[i]][category[5]][name['55']] = ''
                                working_dict[sites[i]][category[5]]['multiplyItemsPrice'] = ''
                                continue

                            elif answer_4[0] == 3:
                                j = 17
                                working_dict[sites[i]][category[5]][name['50']] = '/shopping_cart/checkout/'
                                working_dict[sites[i]][category[5]][name['51']] = '.x-order-product__title'
                                working_dict[sites[i]][category[5]][name['52']] = '.x-order-product__data div:nth-child(2)'
                                working_dict[sites[i]][category[5]][name['53']] = '.x-order-product__price'
                                working_dict[sites[i]][category[5]][name['54']] = '.x-order-total__value span'
                                working_dict[sites[i]][category[5]][name['55']] = '.x-order-total__value span'
                                working_dict[sites[i]][category[5]]['multiplyItemsPrice'] = False
                                working_dict[sites[i]][category[6]][name['60']] = '.x-button_theme_orange'
                                working_dict[sites[i]][category[6]][name['61']] = ''
                                comment('urltmpl', i, 6, 1)
                                count += 7
                                break

                            else:
                                j = 11
                                continue

                        elif answer_3[0] == 0:
                            j += 1
                            continue

                        else:
                            if not flagpps:
                                j = 8
                            else:
                                j = 9
                            count -= 1
                            continue
                    if j == 12:
                        printer1.set(category[5], name['51'], 'copy', 'comment', 'back', working_dict[sites[i]][category[5]][name['51']])
                        delete_comment('inCategory', i, 5, 1)
                        answer_3 = press_key()
                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[5]][name['51']] = answer_3[1]
                            count += 1
                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[5]][name['51']] = ''
                            comment('inCategory', i, 5, 1)
                        elif answer_3[0] == 0:
                            j += 1
                            continue
                        else:
                            j -= 1
                            count -= 1
                            continue
                    if j == 13:
                        printer1.set(category[5], name['52'], 'copy', 'comment', 'back', working_dict[sites[i]][category[5]][name['52']])
                        delete_comment('inCategory', i, 5, 2)
                        answer_3 = press_key()
                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[5]][name['52']] = answer_3[1]
                            count += 1
                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[5]][name['52']] = ''
                            comment('inCategory', i, 5, 2)
                        elif answer_3[0] == 0:
                            j += 1
                            continue
                        else:
                            j -= 1
                            count -= 1
                            continue
                    if j == 14:
                        printer1.set(category[5], name['53'], 'copy', 'comment', 'back', working_dict[sites[i]][category[5]][name['53']])
                        delete_comment('inCategory', i, 5, 3)
                        answer_3 = press_key()
                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[5]][name['53']] = answer_3[1]
                            printer1.set(category[5], 'choose comment', 'one', 'many', 'back', working_dict[sites[i]][category[5]]['multiplyItemsPrice'])
                            answer_4 = press_key2()

                            if answer_4[0] == 1:
                                working_dict[sites[i]][category[5]]['multiplyItemsPrice'] = False
                                count += 1

                            elif answer_4[0] == 2:
                                working_dict[sites[i]][category[5]]['multiplyItemsPrice'] = True
                                count += 1

                            elif answer_4[0] == 0:
                                j += 1
                                continue

                            else:
                                j = 14
                                continue
                                
                        elif answer_3[0] == 0:
                                printer1.set(category[5], 'choose comment', 'one', 'many', 'back', working_dict[sites[i]][category[5]]['multiplyItemsPrice'])
                                answer_4 = press_key2()

                                if answer_4[0] == 1:
                                    working_dict[sites[i]][category[5]]['multiplyItemsPrice'] = False
                                    count += 1

                                elif answer_4[0] == 2:
                                    working_dict[sites[i]][category[5]]['multiplyItemsPrice'] = True
                                    count += 1

                                elif answer_4[0] == 0:
                                    j += 1
                                    continue

                                else:
                                    j = 14
                                    continue

                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[5]][name['53']] = ''
                            comment('inCategory', i, 5, 3)
                            
                        else:
                            j -= 1
                            count -= 1
                            continue
                    if j == 15:
                        printer1.set(category[5], name['54'], 'copy', 'comment', 'back', working_dict[sites[i]][category[5]][name['54']])
                        delete_comment('inCategory', i, 5, 4)
                        answer_3 = press_key()
                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[5]][name['54']] = answer_3[1]
                            count += 1
                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[5]][name['54']] = ''
                            comment('inCategory', i, 5, 4)
                        elif answer_3[0] == 0:
                            j += 1
                            continue
                        else:
                            j -= 1
                            count -= 1
                            continue
                    if j == 16:
                        printer1.set(category[5], name['55'], 'copy', 'comment', 'back', working_dict[sites[i]][category[5]][name['55']])
                        delete_comment('empty', i, 5, 5)
                        answer_3 = press_key()
                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[5]][name['55']] = answer_3[1]
                            count += 1
                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[5]][name['55']] = ''
                            comment('empty', i, 5, 5)
                        elif answer_3[0] == 0:
                            j += 1
                            continue
                        else:
                            j -= 1
                            count -= 1
                            continue
                    if j == 17:
                        printer1.set(category[6], name['60'], 'copy', 'comment', 'back', working_dict[sites[i]][category[6]][name['60']])
                        delete_comment('noPage', i, 6, 0)
                        answer_3 = press_key()
                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[6]][name['60']] = answer_3[1]
                            count += 1
                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[6]][name['60']] = ''
                            working_dict[sites[i]][category[6]][name['61']] = ''
                            comment('noPage', i, 6, 0)
                            break
                        elif answer_3[0] == 0:
                            j += 1
                            continue
                        else:
                            if flag2:
                                j = 11
                            else:
                                j -= 1
                                count -= 1
                            continue
                    if j == 18:
                        printer1.set(category[6], name['61'], 'copy', 'comment', 'back', working_dict[sites[i]][category[6]][name['61']])
                        delete_comment('inCategory', i, 6, 1)
                        answer_3 = press_key()
                        if answer_3[0] == 1:
                            working_dict[sites[i]][category[6]][name['61']] = answer_3[1]
                            count += 1
                        elif answer_3[0] == 2:
                            working_dict[sites[i]][category[6]][name['61']] = ''
                            comment('urltmpl', i, 6, 1)
                        elif answer_3[0] == 0:
                            j += 1
                            continue
                        else:
                            j -= 1
                            count -= 1
                            continue
                    j += 1
                if flag:
                    printer1.set('', 'saved', ' ', ' ', ' ', ' ')
                    set_dict(file_path, working_dict)
                    with open(count_path, 'w', encoding='utf-8') as count_file:
                        count_file.write(str(count))
                    count_file.close()
                    time.sleep(1)
                else:
                    continue
            i += 1
        while True:
            pass


class Interface(threading.Thread):
    def run(self):
        self.root = Tk()

        self.root.title('Работаем')
        self.root.geometry('200x120-0-100')
        self.root.resizable(False, False)

        self.frame1 = Frame(self.root)
        self.frame2 = Frame(self.root)
        self.frame3 = Frame(self.root)
        self.frame5 = Frame(self.root)
        self.frame4 = Frame(self.root)
        

        self.frame1.pack()
        self.frame2.pack()
        self.frame3.pack()
        self.frame5.pack()
        self.frame4.pack(side=LEFT, fill='both', expand=True)
        

        self.label1 = Label(self.frame1, text='1', font='arial 10')
        self.label2 = Label(self.frame2, text='2', font='arial 12')
        self.label3 = Label(self.frame3, text='3', font='arial 14 bold')
        self.label7 = Label(self.frame5, text='4', font='arial 10')
        self.label4 = Label(self.frame4, text='я бобот', bg='green')
        self.label5 = Label(self.frame4, text='я бобот', bg='yellow')
        self.label6 = Label(self.frame4, text='я бобот', bg='red')
        

        self.label1.pack()
        self.label2.pack()
        self.label3.pack()
        self.label7.pack()
        self.label4.pack(side=LEFT, fill='both', expand=True)
        self.label5.pack(side=LEFT, fill='both', expand=True)
        self.label6.pack(side=LEFT, fill='both', expand=True)
        

        self.root.wm_attributes('-topmost', 1)
        self.tick()
        self.root.mainloop()

    def tick(self):
        global text1
        global text2
        global text3
        global text4
        global text5
        global text6
        global text7
        self.label1['text'] = text1
        self.label2['text'] = text2
        self.label3['text'] = text3
        self.label4['text'] = text4
        self.label5['text'] = text5
        self.label6['text'] = text6
        self.label7['text'] = text7
        self.root.after(500, self.tick)


if __name__ == '__main__':
    Tk().withdraw()
    file_path = askopenfilename()
    count_path = file_path.replace('.json', '.txt')
    working_dict = get_dict(file_path)
    sites = list(working_dict.keys())
    printer1 = MyPrinter(len(sites))

    window = Interface()
    logic = Logic()

    logic.daemon = True

    window.start()
    logic.start()
